<?php

require 'vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Migration\YamlFile;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\DependencyFactory;

$config = new YamlFile('config/migrations.yaml');

$conn = DriverManager::getConnection([
    'dbname'   => 'tvi',
    'user'     => 'test',
    'password' => 'test123',
    'host'     => 'tvi_db',
    'driver'   => 'pdo_mysql',
]);

return DependencyFactory::fromConnection($config, new ExistingConnection($conn));