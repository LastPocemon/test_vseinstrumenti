<?php

use App\Kernel;
use App\JsonResponseFormatter;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

define('ROOT_DIR', realpath(__DIR__ . '/..'));
require ROOT_DIR . '/vendor/autoload.php';

try {
    (new Dotenv())->bootEnv(ROOT_DIR . '/.env');

    $container = new ContainerBuilder();
    (new YamlFileLoader($container, new FileLocator(ROOT_DIR)))
        ->load('config/services.yaml');
    $container->compile(true);
    $container->set('container', $container);

    /** @var Kernel $app */
    $app = $container->get('kernel');
    $response = $app->handle(Request::createFromGlobals());
} catch (Exception $e) {
    $response = JsonResponseFormatter::internalServerError();
}

$response->send();