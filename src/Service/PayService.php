<?php


namespace App\Service;


use App\Exception\PayException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;

class PayService {

    /** @var Client */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * @param int $sum
     *
     * @throws PayException
     */
    public function pay(int $sum) {
        try {
            $response = $this->client->get('/');
            if ($response->getStatusCode() !== Response::HTTP_OK) {
                throw new PayException('Error pay');
            }
        } catch (GuzzleException $e) {
            throw new PayException('Error pay');
        }
    }

}