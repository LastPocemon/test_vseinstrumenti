<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\Product;
use App\Exception\OrderException;
use App\Exception\PayException;
use App\Factory\RepositoryFactory;
use App\Repository\OrderProductRepository;
use App\Repository\OrderRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class OrderService {

    /** @var PayService */
    private $payService;

    /**
     * @param PayService $payService
     */
    public function __construct(PayService $payService) {
        $this->payService = $payService;
    }

    /**
     * @param array $idProductsWithQuantity
     *
     * @return int
     * @throws OrderException
     * @throws DBALException
     * @throws ORMException
     * @throws Exception
     */
    public function createNew(array $idProductsWithQuantity): int {
        $idProductToQuantity = $this->prepareIdProductToQuantity($idProductsWithQuantity);

        $productRepository = RepositoryFactory::create(Product::class);

        $products = $productRepository->findBy(['id' => array_keys($idProductToQuantity)]);

        if (count($products) !== count($idProductToQuantity)) {
            throw new OrderException('ID product is not valid');
        }

        $price = $this->priceCalculation($products, $idProductToQuantity);

        $orderRepository = RepositoryFactory::create(Order::class);
        /** @var OrderRepository $orderRepository */
        $idOrder = $orderRepository->createNew($price);

        $orderProductRepository = RepositoryFactory::create(OrderProduct::class);
        try {
            /** @var OrderProductRepository $orderProductRepository */
            $orderProductRepository->saveProducts($idOrder, $idProductToQuantity);
        } catch (Exception $e) {
            $orderRepository->delete($idOrder);
            throw $e;
        }

        return $idOrder;
    }

    /**
     * @param int $idOrder
     * @param int $sum
     *
     * @throws DBALException
     * @throws ORMException
     * @throws OrderException
     */
    public function paid(int $idOrder, int $sum): void {
        $orderRepository = RepositoryFactory::create(Order::class);
        /** @var Order $order */
        $order = $orderRepository->find($idOrder);
        if (is_null($order)) {
            throw new OrderException('ID order is not valid');
        }
        if ($order->getPrice() !== $sum) {
            throw new OrderException('Sum is not valid');
        }
        if ($order->getStatus() !== Order::STATUS_NEW) {
            throw new OrderException('Order is not new');
        }

        try {
            $this->payService->pay($sum);
        } catch (PayException $e) {
            throw new BadRequestException($e->getMessage());
        }

        $orderRepository->isPaid($order);
    }

    /**
     * @param array $idProductsWithQuantity
     *
     * @return array
     * @throws OrderException
     */
    private function prepareIdProductToQuantity(array $idProductsWithQuantity): array {
        $idProductToQuantity = [];
        $uniqueIdProducts = [];

        foreach ($idProductsWithQuantity as $val) {
            if (!is_int($val['quantity']) || $val['quantity'] < 1) {
                throw new OrderException('Quantity product is not valid');
            }
            if (!in_array($val['id'], $uniqueIdProducts)) {
                $uniqueIdProducts[] = $val['id'];
                $idProductToQuantity[$val['id']] = $val['quantity'];
            } else {
                $idProductToQuantity[$val['id']] += $val['quantity'];
            }
        }

        return $idProductToQuantity;
    }

    /**
     * @param array $products
     * @param array $idProductToQuantity
     *
     * @return int
     */
    private function priceCalculation(array $products, array $idProductToQuantity): int {
        $price = 0;
        foreach ($products as $product) {
            /** @var Product $product */
            $price += ($product->getPrice() * $idProductToQuantity[$product->getId()]);
        }
        return $price;
    }
}