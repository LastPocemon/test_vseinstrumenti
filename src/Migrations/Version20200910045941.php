<?php


namespace App\Migrations;


use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200910045941 extends AbstractMigration {

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void {
        $products = [
            [
                'name' => 'Отвертка',
                'price' => 100,
            ],
            [
                'name' => 'Молоток',
                'price' => 150,
            ],
            [
                'name' => 'Киянка',
                'price' => 130,
            ],
            [
                'name' => 'Ножовка по металлу',
                'price' => 200,
            ],
            [
                'name' => 'Набор сверл',
                'price' => 140,
            ],
            [
                'name' => 'Дрель',
                'price' => 560,
            ],
            [
                'name' => 'Шуруповерт',
                'price' => 800,
            ],
            [
                'name' => 'Набор бит',
                'price' => 300,
            ],
            [
                'name' => 'Набор ключей',
                'price' => 600,
            ],
            [
                'name' => 'Шпатель',
                'price' => 150,
            ],
            [
                'name' => 'Ключ разводной',
                'price' => 250,
            ],
            [
                'name' => 'Долото',
                'price' => 200,
            ],
            [
                'name' => 'Кусачки',
                'price' => 190,
            ],
            [
                'name' => 'Мультиметр',
                'price' => 240,
            ],
            [
                'name' => 'набор шестигранников',
                'price' => 300,
            ],
            [
                'name' => 'Углавая шлифмашинка',
                'price' => 1700,
            ],
            [
                'name' => 'Лобзик',
                'price' => 100,
            ],
            [
                'name' => 'Электролобзик',
                'price' => 1900,
            ],
            [
                'name' => 'Топор',
                'price' => 320,
            ],
            [
                'name' => 'Поскогубцы',
                'price' => 300,
            ],
        ];

        foreach ($products as $product) {
            $this->connection->insert(
                'tvi.products',
                $product
            );
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql('TRUNCATE TABLE tvi.products');
    }
}