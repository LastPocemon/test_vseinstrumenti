<?php


namespace App\Migrations;


use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200910104725  extends AbstractMigration {

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void {
        $this->addSql(
            'ALTER TABLE tvi.orders
                ADD COLUMN status VARCHAR(10) NOT NULL,
                ADD COLUMN price INT NOT NULL'
        );

        $this->addSql(
            'ALTER TABLE tvi.orders
                DROP COLUMN is_paid'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql(
            'ALTER TABLE tvi.orders
                DROP COLUMN status,
                DROP COLUMN price'
        );

        $this->addSql(
            'ALTER TABLE tvi.orders
                ADD COLUMN is_paid tinyint(1) NOT NULL DEFAULT 0'
        );
    }
}