<?php


namespace App\Migrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200911081432 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void {
        $this->addSql(
            'ALTER TABLE tvi.orders
                CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql(
            'ALTER TABLE tvi.orders
                CHANGE COLUMN id id INT NOT NULL'
        );
    }
}