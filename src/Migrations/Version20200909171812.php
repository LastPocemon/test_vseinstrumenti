<?php


namespace App\Migrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200909171812 extends AbstractMigration {

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void {
        $this->addSql(
            'CREATE TABLE tvi.orders (
                id INT NOT NULL,
                is_paid tinyint(1) NOT NULL DEFAULT 0,
                PRIMARY KEY (id))'
        );

        $this->addSql(
            'CREATE TABLE tvi.orders_products (
                id_order INT NOT NULL,
                id_product INT NOT NULL,
                quantity INT NOT NULL,
                PRIMARY KEY (id_order,id_product))'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void {
        $this->addSql('DROP TABLE tvi.orders');
        $this->addSql('DROP TABLE tvi.orders_products');
    }
}