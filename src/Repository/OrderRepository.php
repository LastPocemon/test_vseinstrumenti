<?php


namespace App\Repository;


use App\Entity\Order;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class OrderRepository extends EntityRepository {

    /**
     * @param int   $price
     *
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createNew(int $price): int {
        $order = (new Order())
            ->setStatus(Order::STATUS_NEW)
            ->setPrice($price);

        $this->_em->persist($order);
        $this->_em->flush();

        return $order->getId();
    }

    /**
     * @param Order $order
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function isPaid(Order $order): void {
        $order->setStatus(Order::STATUS_PAID);

        $this->_em->persist($order);
        $this->_em->flush();
    }

    /**
     * @param int $idOrder
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(int $idOrder): void {
        $order = $this->find($idOrder);
        $order->setStatus(Order::STATUS_DELETED);

        $this->_em->persist($order);
        $this->_em->flush();
    }

}