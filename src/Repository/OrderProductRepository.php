<?php


namespace App\Repository;


use App\Entity\OrderProduct;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class OrderProductRepository extends EntityRepository {

    /**
     * @param int   $idOrder
     * @param array $idProductToQuantity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveProducts(int $idOrder, array $idProductToQuantity): void {
        foreach ($idProductToQuantity as $idProduct => $quantity) {
            $this->_em->persist(
                (new OrderProduct())
                ->setIdOrder($idOrder)
                ->setIdProduct($idProduct)
                ->setQuantity($quantity)
            );
        }
        $this->_em->flush();
    }

}