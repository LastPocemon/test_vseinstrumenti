<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderProductRepository")
 * @ORM\Table(name="orders_products")
 */
class OrderProduct {

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id_order;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $quantity;

    /**
     * @return int
     */
    public function getIdOrder(): int {
        return $this->id_order;
    }

    /**
     * @param int $idOrder
     *
     * @return OrderProduct
     */
    public function setIdOrder(int $idOrder): self {
        $this->id_order = $idOrder;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdProduct(): int {
        return $this->id_product;
    }

    /**
     * @param int $idProduct
     *
     * @return OrderProduct
     */
    public function setIdProduct(int $idProduct): self {
        $this->id_product = $idProduct;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return OrderProduct
     */
    public function setQuantity(int $quantity): self {
        $this->quantity = $quantity;
        return $this;
    }


}