<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;

class Controller {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self {
        $this->request = $request;
        return $this;
    }
}