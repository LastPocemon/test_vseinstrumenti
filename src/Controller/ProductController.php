<?php

namespace App\Controller;

use App\Entity\Product;
use App\Factory\RepositoryFactory;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;

class ProductController extends Controller {

    /**
     * @return array
     * @throws DBALException
     * @throws ORMException
     */
    public function list() {
        return self::prepareProducts(RepositoryFactory::create(Product::class)->findAll());
    }

    /**
     * @param Product[] $products
     *
     * @return array
     */
    private static function prepareProducts(array $products): array {
        return array_map(
            function($product) {
                /** @var Product $product */
                return $product->toArray();
            },
            $products
        );
    }

}