<?php

namespace App\Controller;


use App\Exception\OrderException;
use App\Service\OrderService;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class OrderController extends Controller {

    /** @var OrderService */
    private $orderService;

    /**
     * @param OrderService $orderService
     */
    public function __construct(
        OrderService $orderService
    ) {
        $this->orderService = $orderService;
    }

    /**
     * @return array
     * @throws DBALException
     * @throws ORMException
     * @throws BadRequestException
     */
    public function create(): array {
        if (!$this->request->attributes->has('products')) {
            throw new BadRequestException('Request body not have param "products"');
        }
        $idProductsWithQuantity = $this->request->attributes->get('products');

        foreach ($idProductsWithQuantity as $value) {
            if (!array_key_exists('id', $value) || !array_key_exists('quantity', $value)) {
                throw new BadRequestException('Request body is not valid');
            }
        }

        try {
            return ['idOrder' => $this->orderService->createNew($idProductsWithQuantity)];
        } catch (OrderException $e) {
            throw new BadRequestException($e->getMessage());
        }
    }

    /**
     * @return array
     * @throws DBALException
     * @throws ORMException
     */
    public function pay(): array {
        if (!$this->request->attributes->has('idOrder')) {
            throw new BadRequestException('Request body not have param "idOrder"');
        }
        if (!$this->request->attributes->has('sum')) {
            throw new BadRequestException('Request body not have param "sum"');
        }

        try {
            $this->orderService->paid(
                $this->request->attributes->get('idOrder'),
                $this->request->attributes->get('sum')
            );
        } catch (OrderException $e) {
            throw new BadRequestException($e->getMessage());
        }

        return [];
    }


}