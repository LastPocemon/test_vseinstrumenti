<?php

namespace App;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Exception;

class Kernel {

    /** @var ContainerBuilder  */
    private $container;

    /**
     * @param ContainerBuilder $container
     */
    public function __construct(ContainerBuilder $container) {
        $this->container = $container;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handle(Request $request): JsonResponse {
        try {
            list($controllerName, $action) = self::resolveRequest($request);
        } catch (ResourceNotFoundException $e) {
            return JsonResponseFormatter::notFound();
        }

        try {
            $this->prepareRequestJsonToArray($request);
        } catch (BadRequestException $e) {
            return JsonResponseFormatter::badRequest($e->getMessage());
        }

        try {
            $responseBody = $this->container->get($controllerName)->setRequest($request)->$action();
        } catch (BadRequestException $e) {
            return JsonResponseFormatter::badRequest($e->getMessage());
        } catch (Exception $e) {
            return JsonResponseFormatter::internalServerError();
        }

        return JsonResponseFormatter::ok($responseBody);
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws ResourceNotFoundException
     */
    private static function resolveRequest(Request $request): array {
        $router = new Router(
            new YamlFileLoader(new FileLocator([ROOT_DIR . '/config'])),
            'routes.yaml',
            ['cache_dir' => ROOT_DIR . '/var/cache'],
            (new RequestContext())->fromRequest($request)
        );

        return explode('::', $router->matchRequest($request)['_controller']);
    }

    /**
     * @param Request $request
     */
    private function prepareRequestJsonToArray(Request &$request) {
        if (!$request->getContent() || $request->getContentType() != 'json') {
            return;
        }

        $body = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestException('Invalid json body - ' . json_last_error_msg());
        }

        $request->attributes->add(is_array($body) ? $body : []);
        $request->request->replace(is_array($body) ? $body : []);
    }
}