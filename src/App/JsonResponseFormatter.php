<?php


namespace App;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseFormatter {

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    public static function ok(array $data): JsonResponse {
        return self::response(
            $data,
            Response::HTTP_OK,
            'OK'
        );
    }

    /**
     * @param string $errorMessage
     *
     * @return JsonResponse
     */
    public static function badRequest(string $errorMessage): JsonResponse {
        return self::response(
            ['error' => $errorMessage],
            Response::HTTP_BAD_REQUEST,
            'Bad Request'
        );
    }

    /**
     * @return JsonResponse
     */
    public static function notFound(): JsonResponse {
        return self::response(
            [],
            Response::HTTP_NOT_FOUND,
            'Not Found'
        );
    }

    /**
     * @return JsonResponse
     */
    public static function internalServerError(): JsonResponse {
        return self::response(
            [],
            Response::HTTP_INTERNAL_SERVER_ERROR,
            'Internal Server Error'
        );
    }

    /**
     * @param array  $data
     * @param int    $code
     * @param string $message
     *
     * @return JsonResponse
     */
    private static function response(array $data, int $code, string $message): JsonResponse {
        return new JsonResponse(
            [
                'data' => $data,
                'message' => $message,
                'code' => $code
            ],
            $code
        );
    }
}