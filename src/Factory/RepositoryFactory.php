<?php

namespace App\Factory;

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Setup;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;

class RepositoryFactory {

    /**
     * @param string $entityName
     *
     * @return EntityRepository
     * @throws DBALException
     * @throws ORMException
     */
    public static function create(string $entityName): EntityRepository {
        return EntityManager::create(
            DriverManager::getConnection([
                'url' => 'mysql://' . $_ENV['MYSQL_USER'] . ':' . $_ENV['MYSQL_PASSWORD'] . '@tvi_db/tvi'
            ]),
            Setup::createAnnotationMetadataConfiguration(
                [ROOT_DIR . '/src/Entity'],
                true,
                null,
                null,
                false
            )
        )->getRepository($entityName);
    }

}